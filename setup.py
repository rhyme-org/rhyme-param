"""PyRhyme setup file"""

import os
from distutils.command.build_py import build_py as _build_py
from setuptools import setup


def read(fname):
    """Reading a given file with its relative path"""
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name='rhymeparam',
    version='2020.1',
    author='Saeed Sarpas',
    author_email='saeed.sarpas@phys.ethz.ch',
    description=
    'Simple package to generate Rhyme parameter files for different scenarios',
    long_description=read('README.md'),
    long_description_content_type="text/markdown",
    url='https://gitlab.com/rhyme-org/rhyme-param.git',
    keywords='Rhyme radiation hydrodynamics simulation parameter files',
    license='GPLv3',
    packages=['rhymeparam'],
    install_requires=[],
    scripts=['scripts/rhyme_param_cold_clump'],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    cmdclass={},
    zip_safe=False,
)
