# Rhyme Param
A simpler way to generate Rhyme parameter files for different scenarios


## Installation

Using `setyp.py`:

```bash
git clone https://gitlab.com/rhyme-org/rhyme-param.git
cd rhyme-param
python3 setup.py install  # --user in case you want to install it locally
```

Using `pip`:

```bash
git clone https://gitlab.com/rhyme-org/rhyme-param.git
cd rhyme-param
pip install .  # --user in case you want to install it locally
pip3 install .  # --user in case you want to install it locally
```

To install the package locally in editable mode, run:

```bash
pip install -e . --user
pip3 install -e . --user
```


## Usage

### Standard Pakcage
Import `rhymeparam` into your script and enjoy ;)

```python
from rhymeparam import RhymeParam as RP
rp = RP(dim=3)

# Setting a cosmology
from rhymeparam.lib.cosmology import PLANCK18
rp.set_cosmology(PLANCK18)

# Print all parameter:
parameters = rp.parameters.all

# Print a single parameter:
parameters.print_param('VersionID')

# Print all simulation-related parameters 
for param in rp.template['main']:
    parameters.print_param(param['name'])
```


## Running tests
To run tests, execute the following command:

```bash
python3 ./setup.py test
```

or

```bash
py.test  # -s to show stdout
```
