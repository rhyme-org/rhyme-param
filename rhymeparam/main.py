"""Rhyme Param -- Generating Rhyme parameter files"""

import copy

from .parameters.parameters_3d import RhymeParameters3d

from .lib import cosmology


class RhymeParam:
    """
    Main class of Rhyme Param

    Handling the general tasks such as:
    - loading proper parameters for different dimensionality
    - assigning preferred cosmology
    - TODO: fill me
    """
    def __init__(self, dim=3):
        """
        Initializing Rhyme Runner

        Args:
            dim: Dimension of the simulation (1, 2, 3)

        Returns:

        Raises:
            RuntimeError: if dimension is other than 3
        """
        self.parameters = {}
        self.template = {}

        if dim == 3:
            self.parameters = RhymeParameters3d()
        else:
            raise RuntimeError('We only support 3d parameters at this point!')

        self._fill_template()

    def set_cosmology(self, cos):
        """
        Setting cosmology

        Args:
            cos: A Cosmology object from lib/cosmology.py module
        """
        if not cosmology.is_valid(cos):
            raise RuntimeError('Cosmology is not a valid cosmology object!')

        cosmo = self.template['Cosmology']

        cosmo['OmegaBaryonNow']['value']['default'][0] = cos['Ob']
        cosmo['OmegaCDMNow']['value']['default'][0] = cos['Om']
        cosmo['OmegaLambdaNow']['value']['default'][0] = cos['Ol']
        cosmo['HubbleConstNow']['value']['default'][0] = cos['H0']

    def _fill_template(self):
        """
        Filling template with the general parameters
        """
        categories = [
            'General',
            'Species',
            'AMR',
            'BC',
            'Cosmology',
            'Thermodynamics',
            'Light_speed',
            'Internal_units',
            'Exact_Riemann_solver',
            'MUSCL-Hancock',
            'Slope_limiter',
            'Ray_tracing',
            'Recombination',
            'Initial_evolution',
            'UVB',
            'Time_step',
            'Output',
            'Logging',
        ]

        for category in categories:
            self.template[category] = copy.deepcopy(
                self.parameters.all[category])
