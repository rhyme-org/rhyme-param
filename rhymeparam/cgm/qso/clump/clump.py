"""Clump related runs"""

from .. import RhymeParamQSO


class RhymeParamClump(RhymeParamQSO):
    """
    Clump class of Rhyme Param

    Handling CGM related parameters
    """
    def __init__(self):
        """
        Initializing RhymeParamClump
        """
        super().__init__()
