"""QSO related runs"""

from .. import RhymeParamCGM


class RhymeParamQSO(RhymeParamCGM):
    """
    QSO class of Rhyme Param

    Handling QSO related parameters
    """
    def __init__(self):
        """
        Initializing RhymeParamQSO
        """
        super().__init__()
