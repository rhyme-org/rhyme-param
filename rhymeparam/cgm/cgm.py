"""CGM related runs"""

from .. import RhymeParam


class RhymeParamCGM(RhymeParam):
    """
    CGM class of Rhyme Param

    Handling CGM related parameters
    """
    def __init__(self):
        """
        Initializing RhymeParamCGM
        """
        super().__init__()

        self.template['CGM'] = []

        self._fill_template()

    def _fill_template(self):
        """
        Filling template with CGM related parameters
        """
        categories = []

        for category in categories:
            for param in self.parameters.all[category].values():
                self.template['CGM'].append(param)
