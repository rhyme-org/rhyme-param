"""Light speed parameters"""

from .. import checkers as check
from .. import new_param

LIGHT_SPEED_PARAMS = {
    'LightSpeedLimit':
    new_param.create('LightSpeedLimit',
        'Prevent superluminal I-front', {
            'default': ['.true.'],
            'desc': ['Light speed limit?'],
            'type': [str],
            'checker': check.if_fortran_boolean,
        }),
}
