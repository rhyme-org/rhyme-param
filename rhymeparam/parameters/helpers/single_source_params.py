"""Single source parameters"""

from .. import checkers as check
from .. import new_param

SINGLE_SOURCE_PARAMS = {
    'NSources':
    new_param.create(
        'NSources', 'Number of sources', {
            'default': [1],
            'desc': ['Number of sources [should be 1]'],
            'type': [int],
            'checker': lambda v: check.if_integer_and_equal_to(v, 1),
        }),
    'SourceCoords':
    new_param.create(
        'SourceCoords', 'Source coordinate in the box length unit', {
            'default': [.5, .5, .5],
            'desc': ['x [L_box]', 'y [L_box]', 'z [L_box]'],
            'type': 3 * [float],
            'checker': check.if_float,
        }),
    'SpectrumType':
    new_param.create(
        'SpectrumType', 'Spectrum type, 1: Power law, 2: Black body', {
            'default': [1],
            'desc': ['Spectrum type'],
            'type': [int],
            'checker': lambda v: check.if_in(v, [1, 2]),
        }),
    'TotalIonRate':
    new_param.create(
        'TotalIonRate',
        'The logarithm of the rate of total ionization photons', {
            'default': [53.7],
            'desc': ['Log([Hz])'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 75)),
        }),
    'SpectrumSlope':
    new_param.create(
        'SpectrumSlope', 'Slope of the power law spectrum (times -1 !!!)', {
            'default': [1.7],
            'desc': ['Power law spectrum slope'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 10)),
        }),
    'OpAngle':
    new_param.create(
        'OpAngle', 'Cosine of the bi-conical opening angle of the source', {
            'default': [0.0],
            'desc': ['Source bi-conical opening angle'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 1)),
        }),
    'NSpectralRegions':
    new_param.create(
        'NSpectralRegions',
        'Number of spectral region (Usually the same as NSpecies)', {
            'default': [3],
            'desc': ['Number of spectral regions'],
            'type': [int],
            'checker': check.if_integer,
        }),
    'NSpectralBins':
    new_param.create(
        'NSpectralBins', 'Number of bins in each spectral region', {
            'default': [10, 10, 10],
            'desc': 3 * ['Number of bins'],
            'type': 3 * [int],
            'checker': check.if_integer,
        }),
    'MinMaxEnergy':
    new_param.create(
        'MinMaxEnergy',
        'Minimum and maximum eneryg of spectral regions (in Rhydberg)', {
            'default': [1.0, 1.8, 1.8, 4.0, 4.0, 10.0],
            'desc': ['Min 1', 'Max 1', 'Min 2', 'Max 2', 'Min 3', 'Max 3'],
            'type': 6 * [float],
            'checker': check.if_float,
        }),
    'InitHIIradius':
    new_param.create(
        'InitHIIradius',
        'Ionized radius around sources in box length unit', {
            'default': [1e-2],
            'desc': ['Initial HII region radius [L_box]'],
            'type': [float],
            'checker': check.if_float,
        },
        note='For outside the box sources, this param regulates the MaxDist')
}
