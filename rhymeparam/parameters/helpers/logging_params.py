"""Logging parameters"""

from .. import checkers as check
from .. import new_param

LOGGING_PARAMS = {
    'Verbosity':
    new_param.create('Verbosity', 'Radamesh logging verbosity (1, 2, 3)', {
        'default': [3],
        'desc': ['Log verbosity level'],
        'type': [int],
        'checker': check.if_integer,
    }),
}
