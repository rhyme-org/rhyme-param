"""3D initial condition parameters"""

from .. import checkers as check
from .. import new_param


def _initial_condition_3d_ic_box_length(value_list):
    """Rules for 3D IC ic_box_length"""
    unit = value_list[3]

    return all(isinstance(v, float)
               for v in value_list[:2]) and isinstance(unit, str)


INITIAL_CONDITION_3D_PARAMS = {
    'ic_type':
    new_param.create(
        'ic_type',
        'Initial condition type, (simple, snapshot)', {
            'default': ['simple'],
            'desc': ['Initial condition type'],
            'type': [str],
            'checker': lambda v: check.if_in(v, ('simple', 'snapshot')),
        },
        sub_params={
            'ic_snapshot_type':
            new_param.create(
                'ic_snapshot_type',
                'Snapshot type (rhyme, radamesh)', {
                    'default': ['rhyme'],
                    'desc': ['Snapshot type'],
                    'type': [str],
                    'checker': lambda v: check.if_in(v, ('rhyme', 'radamesh')),
                    'examples': [['radamesh']],
                },
                dependency=('If ic_type is snapshot',
                           lambda v: check.if_in(v, 'snapshot')),
                sub_params={
                    'ic_snapshot_path':
                    new_param.create(
                        'ic_snapshot_path', 'Path to the snapshot', {
                            'default': ['/path/to/snapshot.h5'],
                            'desc': ['Snapshto path'],
                            'type': [str],
                            'checker': check.if_string,
                        })
                })
        }),
    'ic_grid':
    new_param.create(
        'ic_grid', 'Base grid', {
            'default': [128, 128, 128],
            'desc': ['x', 'y', 'z'],
            'type': 3 * [int],
            'checker': check.if_integer,
        }),
    'ic_box_lengths':
    new_param.create(
        'ic_box_lengths', 'Box lengths', {
            'default': [1.23, 1.23, 1.23, 'kpc'],
            'desc': ['x', 'y', 'z', 'unit'],
            'type': [float, float, float, str],
            'checker': _initial_condition_3d_ic_box_length,
        }),
}
