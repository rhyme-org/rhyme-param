"""Time step parameters"""

from .. import checkers as check
from .. import new_param

TIME_STEP_PARAMS = {
    'InitialSimTime':
    new_param.create('InitialSimTime',
        'Initial simulation time in Myr', {
            'default': [0.0],
            'desc': ['[Myr]'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 1)),
        }),
    'SimulationTime':
    new_param.create('SimulationTime',
        'Final simulation time in Myr (NB. this is not guaranteed)', {
            'default': [50.0],
            'desc': ['[Myr]'],
            'type': [float],
            'checker': check.if_float,
        }),
    'TimeStepFact':
    new_param.create('TimeStepFact',
        'Regulate RT time step based on change in the fastest growing cell', {
            'default': [.2],
            'desc': ['Time step factor'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 1)),
        }),
    'courant_number':
    new_param.create('courant_number',
        'Regulate hydro time step based on CFL condition', {
            'default': [.8],
            'desc': ['Courant number'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 1)),
        }),
    'MinTimeStep':
    new_param.create('MinTimeStep',
        'Minimum value of RT time step (in Myr)', {
            'default': [0.0],
            'desc': ['Myr'],
            'type': [float],
            'checker': check.if_float,
        }),
    'MaxTimeStep':
    new_param.create('MaxTimeStep',
        'Maximum value of RT time step (in Myr)', {
            'default': [1.e3],
            'desc': ['Myr'],
            'type': [float],
            'checker': check.if_float,
        }),
}
