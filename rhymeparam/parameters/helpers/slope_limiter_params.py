"""Rhyme slope limiter parameters"""

from .. import checkers as check
from .. import new_param

SLOPE_LIMITER_PARAMS = {
    'slope_limiter':
    new_param.create('slope_limiter',
        'Slope limiter (van_leer, minmod, van_albada, superbee)', {
            'default': ['minmod'],
            'desc': ['Slope limiter type'],
            'type': [str],
            'checker': lambda v: check.if_in(v, ('van_leer', 'minmod', 'van_albada', 'superbee')),
        }),
    'slope_limiter_omega':
    new_param.create('slope_limiter_omega',
        'Slope limiter omega', {
            'default': [0.0],
            'desc': ['Omega'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (-1, 1)),
        }),
}
