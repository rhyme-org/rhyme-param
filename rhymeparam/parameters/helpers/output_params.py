"""Output parameters"""

from .. import checkers as check
from .. import new_param

OUTPUT_PARAMS = {
    'NTimeSteps':
    new_param.create('NTimeSteps',
        'Number of simulation time steps', {
            'default': [1500],
            'desc': ['Total number of time steps'],
            'type': [int],
            'checker': check.if_integer,
        }),
    'OutputFreqStep':
    new_param.create('OutputFreqStep', 'Output interval in time steps', {
        'default': [1],
        'desc': ['Output frequency [step]'],
        'type': [int],
        'checker': check.if_integer,
    }),
}
