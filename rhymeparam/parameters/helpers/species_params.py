"""Species parameters"""

from .. import checkers as check
from .. import new_param

SPECIES_PARAMS = {
    'NSpecies':
    new_param.create(
        'NSpecies',
                     'Species to be tracked by RT (1: HI, 3: HI, HeI, HeII)', {
                         'default': [3],
                         'desc': ['Number of species'],
                         'type': [int],
                         'checker': check.if_integer,
                     }),
}
