"""Cosmology parameters"""

from .. import checkers as check
from .. import new_param

COSMOLOGY_PARAMS = {
    'OmegaBaryonNow':
    new_param.create(
        'OmegaBaryonNow', 'Omega Baryon at redshift 0', {
            'default': [0.04825],
            'desc': ['Ob'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 2)),
        }),
    'OmegaCDMNow':
    new_param.create(
        'OmegaCDMNow', 'Omega (dark) matter at redshift 0', {
            'default': [0.307],
            'desc': ['Om'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 2)),
        }),
    'OmegaLambdaNow':
    new_param.create(
        'OmegaLambdaNow', 'Omega Lambda at redshift 0', {
            'default': [0.693],
            'desc': ['Ol'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 2)),
        }),
    'HubbleConstNow':
    new_param.create(
        'HubbleConstNow', 'Hubble parameter at redshift 0', {
            'default': [67.77],
            'desc': ['H'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 100)),
        }),
    'InitialRedshift':
    new_param.create(
        'InitialRedshift', 'Initial simulation redshift', {
            'default': [0.0],
            'desc': ['z0'],
            'type': [float],
            'checker': lambda v: check.if_in_range(v, (0, 1100)),
        }),
    'IncludeHubbleExp':
    new_param.create(
        'IncludeHubbleExp', 'Including Hubble expansion', {
            'default': ['.true.'],
            'desc': ['Include Hubble expansion?'],
            'type': [str],
            'checker': check.if_fortran_boolean,
        }),
}
