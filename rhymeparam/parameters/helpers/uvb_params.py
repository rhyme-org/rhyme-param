"""UV background parameters"""

from .. import checkers as check
from .. import new_param

UVB_PARAMS = {
    'UVB_SS':
    new_param.create(
        'UVB_SS', 'UV background self-sheilding density threshold (above which UVB = 0)', {
            'default': ['.false.'],
            'desc': ['UVB self-shielding density threshold'],
            'type': [str],
            'checker': check.if_fortran_boolean,
        }),
    'InitUVBEq':
    new_param.create(
        'InitUVBEq', 'Consider UVB equilibrium during initial evolution', {
            'default': ['.false.'],
            'desc': ['Initiial UVB equilibrium'],
            'type': [str],
            'checker': check.if_fortran_boolean,
        }),
    'KeepBKG':
    new_param.create(
        'KeepBKG', 'Keep UV Background during simulation', {
            'default': ['.false.'],
            'desc': ['Keep UVB'],
            'type': [str],
            'checker': check.if_fortran_boolean,
        }),
}
