"""Rhyme 3D drawing parameters"""

from .. import new_param


def _drawing_3d_canvas_rule(value_list):
    """Rules for canvas parameter"""
    if len(value_list) != 6:
        return False

    typ = value_list[0]

    if typ == 'transparent':
        passed = all(v == '' for v in value_list[1:])
    elif typ == 'uniform':
        passed = all(isinstance(v, float) for v in value_list[1:])
    else:
        passed = False

    return passed


def _drawing_3d_shape_sphere_rule(value_list):
    """Rules for sphere shape parameter"""
    if len(value_list) != 6:
        return False

    return value_list[0] == 'sphere' and all(
        isinstance(v, float)
        for v in value_list[1:5]) and isinstance(value_list[6], str)


def _drawing_3d_shape_filling_rule(value_list):
    """Rules for shape filling parameter"""
    if len(value_list) != 12:
        return False

    return all(isinstance(v, float) for v in value_list[2:]) and all(
        isinstance(v, str) for v in value_list[:2])


DRAWING_3D_PARAMS = {
    'canvas':
    new_param.create(
        'canvas',
        'Drawing canvas',
        {
            'default': ['transparent', '', '', '', '', ''],
            'desc':
            ['Canvas type (transparent, uniform)', 'rho', 'u', 'v', 'w', 'p'],
            'type': [str, float, float, float, float, float],
            'optional': [False, True, True, True, True, True],
            'checker':
            _drawing_3d_canvas_rule,
            'examples': [['uniform', 1.0, 0.0, 0.0, 0.0, 0.1]],
        },
        note='Use internal units for hydro variables!',
    ),
    'shape':
    new_param.create(
        'shape',
        'Drawing a sphere shape on the canvas', {
            'default': ['sphere', 1.23, 2.34, 3.45, 4.56, 0.78, 'kpc'],
            'desc': [
                'shape', 'o_x', 'o_y', 'o_z', 'radius', 'smoothing length',
                'unit'
            ],
            'type': [str, float, float, float, float, float, str],
            'checker':
            _drawing_3d_shape_sphere_rule,
        },
        multi=True,
        sub_params={
            'shape_filling':
            new_param.create(
                'shape_filling',
                'Filling sphere (sp) shape',
                {
                    'default': [
                        'uniform', 'absolute', 1.0, 0.0, 0.0, 0.0, 1.0, .125,
                        0.0, 0.0, 0.0, .1
                    ],
                    'desc': [
                        'Filling type (uniform)',
                        'Filling mode (absolute, add)', 'rho_sp', 'u_sp',
                        'v_sp', 'w_sp', 'p_sp', 'rho_bg', 'u_bg', 'v_bg',
                        'w_bg', 'p_bg'
                    ],
                    'type': [
                        str, str, float, float, float, float, float, float,
                        float, float, float, float
                    ],
                    'checker':
                    _drawing_3d_shape_filling_rule,
                },
                note='Use internal units for hydro variables!',
            ),
        })
}
