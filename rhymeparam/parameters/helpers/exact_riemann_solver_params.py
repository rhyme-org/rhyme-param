"""Rhyme exact Riemann solver parameters"""

from .. import checkers as check
from .. import new_param

EXACT_RIEMANN_SOLVER_PARAMS = {
    'vacuum_pressure':
    new_param.create(
        'vacuum_pressure',
        'Vacuum pressure to be imposed in the exact Riemann solver', {
            'default': [2.2250738585072014E-308],
            'desc': ['Pressure floor [Code unit]'],
            'type': [float],
            'checker': check.if_float,
        }),
    'vacuum_density':
    new_param.create(
        'vacuum_density',
        'Vacuum density to be imposed in the exact Riemann solver', {
            'default': [2.2250738585072014E-308],
            'desc': ['Pressure floor [Code unit]'],
            'type': [float],
            'checker': check.if_float,
        }),
    'tolerance':
    new_param.create(
        'tolerance', 'Tolerance for exact Riemann Solver (obsolete)', {
            'default': [1.e-6],
            'desc': ['Tolerance'],
            'type': [float],
            'checker': check.if_float,
        }),
    'n_iteration':
    new_param.create(
        'n_iteration',
        'Maximum number of Newton\'s iteration to find pressure', {
            'default': [100],
            'desc': ['Number of iterations'],
            'type': [int],
            'checker': lambda v: check.if_integer_and_in_range(v, (5, 1000)),
        }),
}
