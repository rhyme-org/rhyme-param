"""Recombination parameters"""

from .. import checkers as check
from .. import new_param

RECOMBINATION_PARAMS = {
    'CaseA':
    new_param.create('CaseA',
        'Case A recom. (optically thin to ionization photon approximation)', {
            'default': ['.false.'],
            'desc': ['Case A recombination?'],
            'type': [str],
            'checker': check.if_fortran_boolean,
        }),
    'ClumpingFactor':
    new_param.create('ClumpingFactor',
        'Inverse of filling factor, C = <n^2> / <n>^2', {
            'default': [1.0],
            'desc': ['Clumping Factor'],
            'type': [float],
            'checker': check.if_float,
        })
}
