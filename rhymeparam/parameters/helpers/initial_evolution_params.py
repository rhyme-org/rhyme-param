"""Initial evolution parameters"""

from .. import checkers as check
from .. import new_param

INITIAL_EVOLUTION_PARAMS = {
    'DoInitEvol':
    new_param.create(
        'DoInitEvol',
        'Overwrite ionization fractions and T based on equilibrium', {
            'default': ['.false.'],
            'desc': ['Initial evolution?'],
            'type': [str],
            'checker': check.if_fortran_boolean,
        },
        sub_params={
            'InitCollEq':
            new_param.create(
                'InitCollEq', 'Consider collisional excitation equilibrium', {
                    'default': ['.false.'],
                    'desc': ['Initial collisional equilibrium?'],
                    'type': [str],
                    'checker': check.if_fortran_boolean,
                }),
        }
    ),
}
