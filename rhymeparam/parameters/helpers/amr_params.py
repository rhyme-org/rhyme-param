"""Adaptive Mesh Refinement (AMR) parameters"""

from .. import checkers as check
from .. import new_param

AMR_PARAMS = {
    'ic_nlevels':
    new_param.create(
        'ic_nlevels', 'Number of refinement levels (including the base level)',
        {
            'default': [1],
            'desc': ['Number of levels'],
            'type': [int],
            'checker': lambda v: check.if_integer_greater_than(v, 1),
            'examples': [[4]],
        }),
    'max_nboxes':
    new_param.create(
        'max_nboxes', 'Maximum number of refined boxes in each level', {
            'default': [1],
            'desc': ['Maximum number of boxes'],
            'type': [int],
            'checker': lambda v: check.if_integer_greater_than(v, 1),
            'examples': [[1, 3, 9, 27]],
        }),
}
