"""Rhyme MUSCL hancock parameters"""

from .. import checkers as check
from .. import new_param

MUSCL_HANCOCK_PARAMS = {
    'solver_type':
    new_param.create(
        'solver_type',
        'MUSCL-Hancock solver type (cpu_intensive, memory_intensive)', {
            'default': ['memory_intensive'],
            'desc': ['MUSCL-Hancock solver type'],
            'type': [str],
            'checker':
            lambda v: check.if_in(v, ('cpu_intensive', 'memory_intensive')),
        }),
}
