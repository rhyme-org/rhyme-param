"""Rhyme thermodynamics parameters"""

from .. import checkers as check
from .. import new_param

THERMODYNAMICS_PARAMS = {
    'ideal_gas_type':
    new_param.create('ideal_gas_type',
        'Type of ideal gas (monatomic, diatomic, polyatomic)', {
            'default': ['monatomic'],
            'desc': ['Ideal gas type'],
            'type': [str],
            'checker':
            lambda v: check.if_in(v, ('monatomic', 'diatomic', 'polyatomic')),
        }),
}
