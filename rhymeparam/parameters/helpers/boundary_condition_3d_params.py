"""Rhyme boundary condition parameters"""

from .. import checkers as check
from .. import new_param

BOUNDARY_CONDITION_PARAMS = {
    'left_bc':
    new_param.create(
        'left_bc',
        'Left (x-axis) boundary condition, (outflow, reflective, periodic)', {
            'default': ['outflow'],
            'desc': ['Left BC'],
            'type': [str],
            'checker':
            lambda v: check.if_in(v, ('outflow', 'periodic', 'reflective')),
            'examples': [['reflective'], ['periodic']],
        }),
    'right_bc':
    new_param.create(
        'right_bc',
        'Right (x-axis) boundary condition, (outflow, reflective, periodic)', {
            'default': ['outflow'],
            'desc': ['Right BC'],
            'type': [str],
            'checker':
            lambda v: check.if_in(v, ('outflow', 'periodic', 'reflective')),
            'examples': [['reflective'], ['periodic']],
        }),
    'bottom_bc':
    new_param.create(
        'bottom_bc',
        'Bottom (y-axis) boundary condition, (outflow, reflective, periodic)',
        {
            'default': ['outflow'],
            'desc': ['Bottom BC'],
            'type': [str],
            'checker':
            lambda v: check.if_in(v, ('outflow', 'periodic', 'reflective')),
            'examples': [['reflective'], ['periodic']],
        }),
    'top_bc':
    new_param.create(
        'top_bc',
        'Top (y-axis) boundary condition, (outflow, reflective, periodic)', {
            'default': ['outflow'],
            'desc': ['Top BC'],
            'type': [str],
            'checker':
            lambda v: check.if_in(v, ('outflow', 'periodic', 'reflective')),
            'examples': [['reflective'], ['periodic']],
        }),
    'front_bc':
    new_param.create(
        'front_bc',
        'Front (z-axis) boundary condition, (outflow, reflective, periodic)', {
            'default': ['outflow'],
            'desc': ['Front BC'],
            'type': [str],
            'checker':
            lambda v: check.if_in(v, ('outflow', 'periodic', 'reflective')),
            'examples': [['reflective'], ['periodic']],
        }),
    'back_bc':
    new_param.create(
        'back_bc',
        'Back (z-axis) boundary condition, (outflow, reflective, periodic)', {
            'default': ['outflow'],
            'desc': ['Back BC'],
            'type': [str],
            'checker':
            lambda v: check.if_in(v, ('outflow', 'periodic', 'reflective')),
            'examples': [['reflective'], ['periodic']],
        }),
}
