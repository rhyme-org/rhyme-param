"""Internal Rhyme units parameters"""

from .. import checkers as check
from .. import new_param

INTERNAL_UNIT_PARAMS = {
    'density_unit':
    new_param.create(
        'density_unit',
        'Internal Rhyme density unit',
        {
            'default': ['m_H / cm^3'],
            'desc': ['Internal density unit'],
            'type': [str],
            'checker': check.if_string,
        },
        note='Do not change this parameter for RHD or RT simulations!',
    ),
    'length_unit':
    new_param.create(
        'length_unit',
        'Internal Rhyme length unit',
        {
            'default': ['Mpc'],
            'desc': ['Internal length unit'],
            'type': [str],
            'checker': check.if_string,
        },
        note='Do not change this parameter for RHD or RT simulations!',
    ),
    'time_unit':
    new_param.create(
        'time_unit',
        'Internal Rhyme time unit',
        {
            'default': ['Myr'],
            'desc': ['Internal time unit'],
            'type': [str],
            'checker': check.if_string,
        },
        note='Do not change this parameter for RHD or RT simulations!',
    ),
}
