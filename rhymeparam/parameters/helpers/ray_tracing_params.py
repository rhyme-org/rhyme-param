"""Ray tracing parameters"""

from .. import checkers as check
from .. import new_param

RAY_TRACING_PARAMS = {
    'NRays':
    new_param.create('NRays', 'Number of (random) rays per cell', {
        'default': [5],
        'desc': ['Number of rays per cell'],
        'type': [int],
        'checker': check.if_integer,
    }),
    'MaxTau':
    new_param.create('MaxTau',
        'Stop ray tracing above this optical depth (for each specie)', {
            'default': [50.0, 50.0, 50.0],
            'desc': ['HI', 'HeI', 'HeII'],
            'type': 3 * [float],
            'checker': check.if_float,
        }
    ),
}
