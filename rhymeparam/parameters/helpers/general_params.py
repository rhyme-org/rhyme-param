"""General parameters of Rhyme simulations"""

from .. import checkers as check
from .. import new_param

GENERAL_PARAMS = {
    'ActualRun':
    new_param.create('ActualRun',
        'Whether to run the main tiem step loop or not', {
            'default': ['.true.'],
            'desc': ['Actual Run?'],
            'type': [str],
            'checker': check.if_fortran_boolean,
        }),
    'VersionID':
    new_param.create('VersionID',
        'Unique name for the simulation', {
            'default': ['rhyme-simulation'],
            'desc': ['Version ID'],
            'type': [str],
            'checker': check.if_string,
        }),
}
