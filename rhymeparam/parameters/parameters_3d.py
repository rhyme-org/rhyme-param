"""3D Rhyme simulation parameters"""

from .parameters import RhymeParameters

from .helpers.boundary_condition_3d_params import BOUNDARY_CONDITION_PARAMS
from .helpers.initial_condition_3d_params import INITIAL_CONDITION_3D_PARAMS
from .helpers.drawing_3d_params import DRAWING_3D_PARAMS


class RhymeParameters3d(RhymeParameters):
    """Rhyme parameters for 3D simulations"""
    def __init__(self):
        self.all = {}

        super().__init__()

        self.all['BC'] = BOUNDARY_CONDITION_PARAMS
        self.all['Initial_condition'] = INITIAL_CONDITION_3D_PARAMS
        self.all['Drawing'] = DRAWING_3D_PARAMS
