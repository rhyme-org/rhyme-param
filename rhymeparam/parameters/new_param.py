"""Generating a dictionary for a given param"""

_ITERABLES = (list, tuple)


def create(name,
           desc,
           value,
           dependency=None,
           note=None,
           multi=False,
           sub_params=None):
    """
    Generating a new dictionary describing a parameter

    Args:
        neme: Parameter name
        desc: Description of the parameter
        value: A dictionary containing at least:
            - default: default value(s) as a list
            - desc: description of the value(s)
            - type: value type(s) as a list
            - checker: a checker function to evaluate the value(s)
            - optional: specify whether the value is optional or not (optional)
            - examples: A list of example values (optional)
        dependency: A tuple (str, func) which specify if the parameter depends
            on the value of other parameters
        nb: N.n. Nota bene
        multi: Whether or not the parameter can be repeated
        sub_params: A dictionary of child parameters
    """

    if not all(key in value for key in ['default', 'desc', 'type', 'checker']):
        raise RuntimeError(
            'value should conitan default, desc, type and checker lists!')

    if not isinstance(value['default'], (list, tuple)):
        raise RuntimeError('default value(s) must be a list!')

    if not isinstance(value['desc'], (list, tuple)):
        raise RuntimeError('desc of value(s) must be a list!')

    if len(value['default']) != len(value['desc']):
        raise RuntimeError(
            'desc of value(s) must have the same length as default value list!'
        )

    if not isinstance(value['type'], (list, tuple)):
        raise RuntimeError('type of value(s) must be a list!')

    if len(value['default']) != len(value['type']):
        raise RuntimeError(
            'type of value(s) must have the same length as default_value!')

    if not callable(value['checker']):
        raise RuntimeError('checker of value(s) must be a function!')

    new_parameter = {
        'name': str(name),
        'desc': str(desc),
        'value': {
            'default': value['default'],
            'desc': value['desc'],
            'type': value['type'],
            'checker': value['checker'],
            'optional': None,
            'examples': None,
        },
        'dependency': dependency,
        'N.b.': note,
        'multi': multi,
        'sub_params': sub_params
    }

    if 'optional' in value and isinstance(value['optional'], (list, tuple)):
        if len(value['default']) == len(value['optional']):
            new_parameter['value']['optional'] = value['optional']
        else:
            raise RuntimeError(
                'value[optional] must have the same length as value[default]')

    if 'examples' in value:
        exmpls = value['examples']

        if isinstance(exmpls, _ITERABLES) and isinstance(
                exmpls[0], _ITERABLES):
            new_parameter['value']['examples'] = value['examples']

    if dependency is not None:
        if isinstance(dependency, _ITERABLES):
            if len(dependency) == 2:
                if isinstance(dependency[0], str) and callable(dependency[1]):
                    new_parameter['dependency'] = dependency
                else:
                    raise RuntimeError(
                        'Condition members must be (str, function)')
            else:
                raise RuntimeError('Length of dependency must be 2!')
        else:
            raise RuntimeError('Condition must be an iterable!')

    return new_parameter
