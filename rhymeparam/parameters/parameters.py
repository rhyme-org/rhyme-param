"""General Rhyme Simulation parameters"""

from .helpers.general_params import GENERAL_PARAMS
from .helpers.amr_params import AMR_PARAMS
from .helpers.light_speed_params import LIGHT_SPEED_PARAMS
from .helpers.initial_evolution_params import INITIAL_EVOLUTION_PARAMS
from .helpers.species_params import SPECIES_PARAMS
from .helpers.uvb_params import UVB_PARAMS
from .helpers.output_params import OUTPUT_PARAMS
from .helpers.logging_params import LOGGING_PARAMS
from .helpers.time_step_params import TIME_STEP_PARAMS
from .helpers.cosmology_params import COSMOLOGY_PARAMS
from .helpers.single_source_params import SINGLE_SOURCE_PARAMS
from .helpers.recombination_params import RECOMBINATION_PARAMS
from .helpers.internal_unit_params import INTERNAL_UNIT_PARAMS
from .helpers.thermodynamics_params import THERMODYNAMICS_PARAMS
from .helpers.exact_riemann_solver_params import EXACT_RIEMANN_SOLVER_PARAMS
from .helpers.slope_limiter_params import SLOPE_LIMITER_PARAMS
from .helpers.muscl_hancock_params import MUSCL_HANCOCK_PARAMS
from .helpers.ray_tracing_params import RAY_TRACING_PARAMS

_INDENT = '    '


class RhymeParameters:
    """General Rhyme parameters"""
    def __init__(self):
        """
        Initializing RhymeParameters class
        """
        self.all = {
            'General': GENERAL_PARAMS,
            'AMR': AMR_PARAMS,
            'Light_speed': LIGHT_SPEED_PARAMS,
            'Initial_evolution': INITIAL_EVOLUTION_PARAMS,
            'Species': SPECIES_PARAMS,
            'Recombination': RECOMBINATION_PARAMS,
            'UVB': UVB_PARAMS,
            'Output': OUTPUT_PARAMS,
            'Logging': LOGGING_PARAMS,
            'Time_step': TIME_STEP_PARAMS,
            'Cosmology': COSMOLOGY_PARAMS,
            'Single_source': SINGLE_SOURCE_PARAMS,
            'Internal_units': INTERNAL_UNIT_PARAMS,
            'Thermodynamics': THERMODYNAMICS_PARAMS,
            'Exact_Riemann_solver': EXACT_RIEMANN_SOLVER_PARAMS,
            'Slope_limiter': SLOPE_LIMITER_PARAMS,
            'MUSCL-Hancock': MUSCL_HANCOCK_PARAMS,
            'Ray_tracing': RAY_TRACING_PARAMS,
            'BC': None,  # TBF
            'Initial_condition': None,  # TBF
            'Drawing': None,  # TBF
        }

    def print_param(self, name, category=None):
        """
        Retrieving and printing a parameter

        Args:
            name: Parameter name
            category: Category that the parameter belongs to (optional)
        """
        category, param = self._get_param(name, category)

        if param:
            _print_param(param)
            _print_sub_params(param['sub_params'])
        else:
            print('[Error] Unable to find parameter ' + name)

    def print_category(self, category):
        """
        Printing all parameters within a given category

        Args:
            category: Name of the category
        """
        if category in self.all:
            for param_name in self.all[category].keys():
                self.print_param(param_name, category=category)
        else:
            print('[Error] Unknown category: ' + category)

    def print_all_params(self):
        """
        Printing all parameters of self.all
        """
        for category_name in self.all:
            self.print_category(category_name)

    def _get_param(self, name, category=None):
        """
        Retrieving a parameters from the dictionary of all parameters

        Args:
            name: Parameter name
            category: Category that the parameter belongs to (optional)

        Raises:
            KeyError: If unable to find the param
        """
        param = None

        if category:
            try:
                param = self.all[category][name]
            except KeyError:
                pass
        else:
            for ctgry_name, ctgry in self.all.items():
                if name in ctgry:
                    category = ctgry_name
                    param = ctgry[name]

        return category, param


def _get_param_indent(level):
    """Returning indentation variables"""
    return (level - 1) * _INDENT, '--- ', _INDENT


def _format_param_name(name, level=1):
    """Return formatted parameter name"""
    base_indnt, _, _ = _get_param_indent(level)

    return '%s**%s**' % (base_indnt, str(name))


def _format_param_description(description, level=1):
    """Return formatted parameter description"""
    base_indnt, indnt, _ = _get_param_indent(level)

    return '%s|%sdescription: %s' % (base_indnt, indnt, description)


def _format_param_note(note, level=1):
    """Return formatted parameter N.b. message"""
    base_indnt, indnt, _ = _get_param_indent(level)

    return '%s|%sNote that: %s' % (base_indnt, indnt, note)


def _format_param_dependency(dependency, level=1):
    """Return formatted parameter dependency"""
    base_indnt, indnt, _ = _get_param_indent(level)
    cnd = dependency[0] if dependency is not None else 'None'

    return '%s|%sDependency: %s' % (base_indnt, indnt, cnd)


def _format_param_default_values(name, values, level=1):
    """Return formatted parameter default values"""
    base_indnt, indnt, _ = _get_param_indent(level)
    default_values_str = ' '.join([str(v) for v in values])

    return '%s|%sdefault: %s = %s' % (base_indnt, indnt, name,
                                      default_values_str)


def _format_param_value_descriptions(descriptions, optional, level=1):
    """Return formatted description of parameter values"""
    base_indnt, indnt, empty_indnt = _get_param_indent(level)

    if optional is not None:
        decorations = [('[', ']') if opt else ('', '') for opt in optional]
    else:
        decorations = [('', '') for _ in descriptions]

    descriptions_list = [
        '%s|%s- %s%d%s: %s' %
        (base_indnt, empty_indnt, decor[0], n + 1, decor[1], desc)
        for n, desc, decor in zip(range(len(descriptions)), descriptions,
                                  decorations)
    ]

    return '%s|%sValue descriptions ([] means optional)\n%s' % (
        base_indnt, indnt, '\n'.join(descriptions_list))


def _format_param_examples(name, examples, level=1):
    """Return formatted parameter examples"""
    examples_list = []

    if examples is not None:
        base_indnt, indnt, _ = _get_param_indent(level)

        for example in examples:
            examples_list.append(
                '%s|%sExample: %s = %s' %
                (base_indnt, indnt, name, ' '.join(list(map(str, example)))))

    return '\n'.join(examples_list)


def _format_param_sub_params(level=1):
    """Return formatted separator for sub-parameters"""
    if level == 0:
        return ''

    base_indnt, indnt, _ = _get_param_indent(level)

    base_indnt = len(base_indnt) * ' '
    indnt = '└' + (len(indnt) - 1) * '-'

    return base_indnt + indnt + ' Dependnet parameters'


def _print_param(param, level=1):
    """
    Print a parameter based on a given name

    Args:
        param: The parameter to be printed
        level: Level of the parameters in a nested parameters
    """
    if not param:
        return

    print(_format_param_name(param['name'], level=level))
    print(_format_param_description(param['desc'], level=level))
    print(_format_param_note(param['N.b.'], level=level))
    print(_format_param_dependency(param['dependency'], level=level))
    print(
        _format_param_value_descriptions(param['value']['desc'],
                                         param['value']['optional'],
                                         level=level))
    print(
        _format_param_default_values(param['name'],
                                     param['value']['default'],
                                     level=level))
    print(
        _format_param_examples(param['name'],
                               param['value']['examples'],
                               level=level))


def _print_sub_params(params, level=2):
    """Printing sub parameters"""
    if not params:
        return

    print(_format_param_sub_params(level=level - 1))

    for param in params.values():
        _print_param(param, level=level)
        _print_sub_params(param['sub_params'], level=level + 1)
