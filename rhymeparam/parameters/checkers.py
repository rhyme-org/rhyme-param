"""Functions to check parameters"""


def if_fortran_boolean(var):
    """Check if the string variables are valid Fortran boolean"""
    return all(v in ['.true.', '.false.'] for v in var)


def if_string(var):
    """Check if the variables are strings"""
    return all(isinstance(v, str) for v in var)


def if_integer(var):
    """Check if the variables are integers"""
    return all(isinstance(v, int) for v in var)


def if_integer_and_equal_to(var, val):
    """Check if the variables are integers and equal to a given value"""
    return all(isinstance(v, int) and v == val for v in var)


def if_integer_greater_than(var, val):
    """Check if the variables are integers and greater than a given value"""
    return all(isinstance(v, int) and v > val for v in var)


def if_integer_and_in_range(var, rng):
    """Check if the variables are integers and withing a given range"""
    return all(isinstance(v, int) and rng[0] <= v <= rng[1] for v in var)


def if_float(var):
    """Check if the variables are floats"""
    return all(isinstance(v, float) for v in var)


def if_in(var, lst):
    """Check if the variables are members of a given list"""
    return all(v in lst for v in var)


def if_in_range(var, rng):
    """Check if the varibales are within a given range"""
    return all(rng[0] <= v <= rng[1] for v in var)
